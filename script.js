console.log("DOM MANIPULATION");
/*
  This is a Webpage

  <html>
  <head>
    <title></title>
  </head>
  <body>
    <div>
      <h1></h1>
      <p></p>
    </div>
  </body>
  </html>
*/

/* DOM
      document : {
        html: {
          head: {
            title: {
            },
            meta: {
            }
          },
          body: {
            div: {
              h1: {
                
              }
              p: {
              }
            }
          }
        }
      }

*/

const paragraph = document.getElementById("demo");
// console.log(paragraph);
// paragraph.textContent = `hello`;

const myh1 = document.querySelector(".title");
// console.log(document.getElementsByClassName("title"));
// console.log(document.querySelector("#demo"));
// console.log(document.querySelector(".title"));
// console.log(document.querySelector("h1"));

// myh1.innerHTML = `Hello World`;
// set attribute
myh1.setAttribute("style", "background-color: red;");
// remove attribute
myh1.removeAttribute("style");

// element.style.property
myh1.style.color = "red";

// event listeners
myh1.addEventListener("click", () => console.log(`hehe`));

const firstName = document.querySelector("#first-name");
const lastName = document.querySelector("#last-name");
const fullName = document.querySelector("#full-name");
// const btn = document.querySelector(".btn");
// firstName.addEventListener("keyup", (event) => {
//   fullName.textContent = event.target.value;
// });

// lastName.addEventListener("keyup", (event) => {
//   fullName.textContent = event.target.value;
// });
const updateName = () => {
  const first = firstName.value;
  const last = lastName.value;

  fullName.innerHTML = `${first} ${last}`;
};

firstName.addEventListener("keyup", updateName);
lastName.addEventListener("keyup", updateName);

// btn.addEventListener("click", () => {
//   fullName.textContent = `${firstName} ${lastName}`;
// });
